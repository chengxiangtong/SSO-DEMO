package com.yao2san.appa.home.controller;

import com.yao2san.appa.contstant.AppConstant;
import com.yao2san.appa.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by wxg on 2019/5/4 20:36
 */
@Controller
public class HomeController {
    @Autowired
    private RestTemplate restTemplate;
    @RequestMapping("/home")
    public String home(HttpSession session, Model model) {
        model.addAttribute("userInfo", session.getAttribute("userInfo"));
        return "/home";
    }
    @ResponseBody
    @RequestMapping("/logout")
    @SuppressWarnings("all")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        //获取ticker
        String ticket = CookieUtil.getCookie(request,"ticket");
        //请求sso-server退出登录
        boolean isLogout = restTemplate.getForObject(AppConstant.SSO_URL+"logout?ticket="+ticket,Boolean.class);
        if(isLogout){
            //清除cookie
            CookieUtil.setCookie(response,"ticket","",0);
            return "用户已登出";
        }
        return "登出失败";
    }
}
